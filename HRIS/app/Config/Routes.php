<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}


/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
// $routes->setDefaultController('Home');
// $routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::Index');
$routes->get('/attendance', 'Attendance::Index');
$routes->get('/approval', 'Approval::Index');
$routes->get('/referensi/RefGapok', 'Referensi\RefGapok::GetData');
$routes->get('/notification', 'Notification::Index');
$routes->get('/personal', 'Personal::Index');
$routes->get('/request', 'Request::Index');
$routes->get('/Request/cuti', 'Cuti::Index');
$routes->get('/Request/pinjam', 'Pinjam::Index');
$routes->get('/Request/resign', 'Resign::Index');


// Services
// == Referensi

// == Referensi Gapok
$routes->get('/referensi/RefGapok/getData/(:segment)', 'Referensi\RefGapok::GetData/$1');
$routes->post('/referensi/RefGapok/saveData', 'Referensi\RefGapok::SaveData');
// Referensi Gapok == 

// == Referensi Permohonan Cuti
$routes->get('/referensi/RefCuti/getData/(:segment)', 'Referensi\RefCuti::GetData/$1');

// == Referensi Permohonan Resign
$routes->get('/referensi/RefResign/getData/(:segment)', 'Referensi\RefResign::GetData/$1');

// == Referensi Permohonan Resign
$routes->get('/referensi/RefPinjaman/getData/(:segment)', 'Referensi\RefPinjaman::GetData/$1');

// Referensi == 

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
