<?php

namespace App\Controllers\Referensi;
use App\Controllers\BaseController;

use App\Models\RefPinjamanModel;

class RefPinjaman extends BaseController
{
    protected $refGapokModel;
    public function __construct(){
        $this->refPinjamanModel = new RefPinjamanModel();
    }

    public function GetData($id = false){
        $data = $this->refPinjamanModel->getData($id);
        dd($data);
    }

}