<?php

namespace App\Controllers\Referensi;
use App\Controllers\BaseController;

use App\Models\RefGapokModel;

class RefGapok extends BaseController
{
    protected $refGapokModel;
    public function __construct(){
        $this->refGapokModel = new RefGapokModel();
    }

    public function GetData($id = false){
        $data = $this->refGapokModel->getData($id);
        return $data;
    }

    public function SaveData($id = false){
        $data = ['test'];
        $parameter = $this->request->getJSON();
        echo json_encode($parameter);
    }
}