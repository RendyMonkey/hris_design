﻿    <div class="container">
        <main role="main">
            <div class="profile-content pt-3">
                <div class="row text-center">
                    <img src="../assets/images/users/user-1.jpg" alt="" class="rounded-circle" style="display: block; margin:auto;">
                </div>
                <h6 class="profile-name-custom">Nancy Wicaksono</h6>
            </div>
            
            <div class="row mt-3">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body text-center pt-3">
                            <p class="header-title-custom mb-0">Payroll Info</p>
                            <hr class="mb-1 mt-1" />
                            <p class="text-custom2 m-0">November, 2021</p>
                            <p class="text-custom2 m-0">Rp.XXXXXX <i class="far fa-eye"></i></p>
                            <p></p>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-body text-center pt-3">
                            <p class="header-title-custom mb-0">Timesheet Schedule</p>
                            <hr class="mb-1 mt-1" />
                            <p class="text-custom2 m-0">Sunday, 20 Feb 2022</p>
                            <p class="text-custom2 m-0">HOLIDAY</p>
                            <p class="text-custom2 m-0">00:00 s\d 00:00</p>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row mt-0" style="margin-top: 0!important;">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body text-center pt-3">
                            <p class="header-title-custom mb-0">Personal Data</p>
                            <hr class="mb-1 mt-1" />
                            <table align="center" style="margin:auto;">
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold" style="width: 49%;">Sys ID</td>
                                    <td class="text-center text-custom2 px-1" style="width: 2%;">:</td>
                                    <td class="text-left text-custom2" style="width: 49%;">95</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Employee ID</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">0510-120601-0139</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Full Name</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Nancy Wicaksono</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Nick Name</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Wicak</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Job Title</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Dangdut Singer</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Company</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Cv.Pantura Indonesia</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Base Payroll</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Head Quarter</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Base Location</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Head Quarter</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Organization Code</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">1.1.1.1.1.4</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Organization Name</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Finance & Accounting Head</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Employee Position</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Singer</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Employee Status</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">Intern</td>
                                </tr>
                                <tr>
                                    <td class="text-right text-custom2 font-weight-bold">Entry Date</td>
                                    <td class="text-center text-custom2">:</td>
                                    <td class="text-left text-custom2">2001-07-12</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body text-center pt-3">
                            <p class="header-title-custom mb-0">Attendance Overview</p>
                            <hr class="mb-3 mt-1" />
                            <p class="text-custom2 m-0">February, 2022</p>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-body text-center pt-3">
                            <p class="header-title-custom mb-0">Leave Credit</p>
                            <hr class="mb-3 mt-1" />
                            <p class="text-custom2 m-0">Sunday, 20 Feb 2022</p>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
